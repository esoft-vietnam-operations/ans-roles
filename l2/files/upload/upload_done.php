<?php

$tmpFile = $_SERVER['HTTP_X_FILE'];
$tmpFileMD5 = $_SERVER['HTTP_CONTENT_MD5'];

if (!is_string($tmpFile) || strlen($tmpFile) < 1 || !is_string($tmpFileMD5) || strlen($tmpFileMD5) < 1) {
  unlink($tmpFile);
  header('HTTP/1.1 403 Forbidden', true, 403);
  header("Content-Type: application/json");
  die("{\"errorMsg\": \"file not stored correct or http header Content-MD5 not set!\"}");
}

$calculatedMD5 = md5_file($tmpFile, false);
if (strcmp($tmpFileMD5, $calculatedMD5) != 0) {
  unlink($tmpFile);
  header('HTTP/1.1 403 Forbidden', true, 403);
  header("Content-Type: application/json");
  die("{\"errorMsg\": \"calculated md5 not as expected:'" . $calculatedMD5 . "'\"}");
}

$chunks = str_split($calculatedMD5, 2);
$pathParts = array_slice($chunks, 0, 3);
array_push($pathParts, "".implode("", array_slice($chunks, 3)));

$fileLocation = "files/".implode(DIRECTORY_SEPARATOR, $pathParts);
$filePath = "/data/".$fileLocation;
$parentDir = dirname($filePath);
if (!is_dir($parentDir)) {
#	$oldUmask = umask(0);
/* This sets the default permissions for user, groups, and others respectively:

  • 0 - read, write and execute
  • 1 - read and write
  • 2 - read and execute
  • 3 - read only
  • 4 - write and execute
  • 5 - write only
  • 6 - execute only
  • 7 - no permissions
*/
  $oldUmask = umask(0);
	$mkdirResult = mkdir($parentDir, 0775, true);
	umask($oldUmask);
    if (!$mkdirResult) {
      unlink($tmpFile);
      header('HTTP/1.1 500 Internal Server Error', true, 500);
      header("Content-Type: application/json");
      die("{\"errorMsg\": \"could not create files ".$filePath." folder - please try again: ".$parentDir."\"}");
    }
}


if (!rename($tmpFile, $filePath)) {
  unlink($tmpFile);
  header('HTTP/1.1 500 Internal Server Error', true, 500);
  header("Content-Type: application/json");
  die("{\"errorMsg\": \"could not move tmp file to files folder ".$filePath." - please try again\"}");
}

chmod($filePath, 0666);

header('HTTP/1.1 201 Created', true, 201);
header("Location: " . $fileLocation);
