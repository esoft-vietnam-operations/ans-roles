# Common

### Platform

- Ubuntu 20.4

### Role description

- This role will make some first configuration, install needed packages.
- We also run optinal tasks from Utils, including tasks about Docker, mour data disk, installing zsh and setting dns.

### Dependencies
- To run task related to docker, you have to install docker first.

### Main tasks

- Tasks:
    - packages
    - time
    - users
    - zsh


- Variables with default value
    - `time_zone`
    - `basic_pkgs` # List baisc packages will be instlled
    - `utils_pkgs` # List utils packages will be installed
    - `zsh_manage_user` # List user will be installed zsh
    - `zsh_dir` # zsh_dir # zsh template direcroty path in Ansible Controller Server
    - `zsh_file` # zsh file path in Ansbile Controller Server
    - `zsh_repo` # zsh tempale repo 
 
- Variables must be defined     
    - `root_password` # New password for root
    - `cloud_user_password` # New pssword for cloud-user
    - `cloud_user_auth_key` # New contents of auth key for cloud-user
    - `root_auth_key` # New contents of auth key for root


### Utils tasks

- Tasks:
    - docker_login
    - docker_prereqs
    - mount_datadisk
    - static_dns_server


- docker_prereqs - Varibales must be defined
    - `container_user_password` # Password for container-user

- docker_login - Varibales must be defined
    - `docker_username`
    - `docker_email`
    - `docker_password`


## How to call role
- Call common
```
- role:
    - common
```

- Call tasks from Common/Utils

```
tasks:
    - include_role:
        name: common/utils
        tasks_from: "{{ item }}"
      with_items:
        - docker_prereqs
        - docker_login
        - mount_datadisl
        - static_dns_server
```



## Bitucket 

* [common](https://bitbucket.org/axoninsightdevops/axidpp-plybks/src/master/roles) - Bitbucket Repo

