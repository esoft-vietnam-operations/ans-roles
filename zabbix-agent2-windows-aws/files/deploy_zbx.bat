:: Installing Zabbix Agent 2 Service on host

@ECHO OFF

echo Installing Zabbix Agent 2 Service
cd C:\Program Files\Zabbix Agent 2\bin
zabbix_agent2.exe --config "C:\Program Files\Zabbix Agent 2\conf\zabbix_agent2.conf" --install