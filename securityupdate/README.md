1. Enable auto-upgrade security packages and restart server  


2. Determining the current configuration
apt-config dump APT::Periodic::Unattended-Upgrade

- Which will produce output like:

APT::Periodic::Unattended-Upgrade "1";

In this example, Unattended Upgrade will run every 1 day. If the number is "0" then unattended upgrades are disabled.

3. Check if it works

sudo unattended-upgrades --dry-run --debug

- check log
cat /var/log/unattended-upgrades/unattended-upgrades.log
