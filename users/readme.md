### Supported Groups
- `opsusers`
- `vnengineeringusers`
- `dkresdevusers`

### Variables
- `create_opsusers` (default: `true`)
- `create_vnengineeringusers` (default: `false`)
- `create_dkresdevusers` (default: `false`)
